﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductProperties
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Product Name: ");
            string pName = Console.ReadLine();
            Console.WriteLine("Enter Product Price: ");
            int pPrice = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Product Color: ");
            string pColor = Console.ReadLine();
            Console.WriteLine("Enter Product Size: ");
            int pSize = int.Parse(Console.ReadLine());
            string value = $"Product Name: {pName}, Product Price: {pPrice}, Product Color: {pColor}, Product Size:  {pSize} ";
            Console.WriteLine(value);
        }
    }
}
